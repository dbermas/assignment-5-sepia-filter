#include "../include/bmp.h"
#include "../include/deserializer.h"
#include "../include/serializer.h"
#include <stdio.h>



#ifndef IMAGE_ROTATION_BMPREADER_H
#define IMAGE_ROTATION_BMPREADER_H



enum read_status openBMP(char const* fileName);
enum write_status writeBMP(char const* fileName);

#endif
