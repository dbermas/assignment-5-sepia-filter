#include "../include/image.h"
#include <stdio.h>


#ifndef IMAGE_ROTATION_DESERIALIZER_H
#define IMAGE_ROTATION_DESERIALIZER_H


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR,
    MEMORY_ALLOCATION_ERROR
};

enum read_status fromBMP( FILE* in, struct image* img );
#endif //IMAGE_ROTATION_DESERIALIZER_H
