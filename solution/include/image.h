#include "../include/pixel.h"
#include <stdint.h>


#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H


struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image createImage(uint64_t const width, uint64_t const height);

#endif //IMAGE_ROTATION_IMAGE_H
