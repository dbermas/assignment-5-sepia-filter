#include <stdint.h>

#ifndef IMAGE_ROTATION_PIXEL_H
#define IMAGE_ROTATION_PIXEL_H


struct pixel {
    uint8_t blue, green, red;
};
#endif //IMAGE_ROTATION_PIXEL_H
