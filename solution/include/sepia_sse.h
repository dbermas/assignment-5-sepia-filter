
#ifndef ASSIGNMENT_5_SEPIA_FILTER_SEPIA_SSE_H
#define ASSIGNMENT_5_SEPIA_FILTER_SEPIA_SSE_H
#include "image.h"
extern void sepia_asm(struct image* img, struct image* sepiaImg);
#endif //ASSIGNMENT_5_SEPIA_FILTER_SEPIA_SSE_H
