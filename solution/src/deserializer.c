
#include "../include/bmp.h"
#include "../include/deserializer.h"

#include <malloc.h>


#define BMP_TYPE 0x4D42
#define BMP_BIT_COUNT 24





enum read_status fromBMP(FILE* in, struct image* img ){
    struct bmpHeader bmpHeader;

    if (!fread(&bmpHeader,sizeof(struct bmpHeader),1,in)){
        return READ_ERROR;
    }

    if (bmpHeader.bfType != BMP_TYPE){
        return READ_INVALID_SIGNATURE;
    }

    if (bmpHeader.biBitCount != BMP_BIT_COUNT){
        return READ_INVALID_BITS;
    }

    img->height = bmpHeader.biHeight;
    img->width = bmpHeader.biWidth;

    img->data = (struct pixel*)malloc(sizeof(struct pixel) * bmpHeader.biWidth * bmpHeader.biHeight);
    if (img->data==NULL){

        return MEMORY_ALLOCATION_ERROR;
    }

    uint8_t padding = countPadding(img->width*sizeof(struct pixel));
    for (uint64_t i = 0; i < img->height; i++) {
        if(fread(&img->data[i*img->width],sizeof(struct pixel),img->width,in)!=img->width){
            free(img->data);
            return READ_INVALID_HEADER;
        }
        fseek(in,padding,SEEK_CUR);
    }

    return READ_OK;
}

