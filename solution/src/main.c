#include "../include/bmpIO.h"
#include "../include/deserializer.h"
#include "../include/sepia_c.h"
#include "../include/sepia_sse.h"
#include "../include/serializer.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(int argc, char *argv[]) {

    if (argc != 3) {
        fprintf(stderr, "Syntax: bmpreader <bmp file> <destination file>\n");

        return 0;
    }


    char *fileName = argv[1];
    struct image openImage;
    enum read_status openBMPStatus = openBMP(fileName);

    FILE *in = 0;
    if (openBMPStatus == READ_OK) {
        in = fopen(fileName, "rb");
    } else {
        fclose(in);
        exit(1);
    }

    enum read_status openImageStatus = fromBMP(in, &openImage);
    if (openImageStatus != READ_OK) {
        free(openImage.data);
        exit(1);
    }
    fclose(in);


    clock_t begin = clock();
    struct image cSepiaImage = makeSepia(openImage);
    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("c execution %f\n", time_spent);

    struct image asmImg = createImage(openImage.width, openImage.height);

    clock_t beginASM = clock();
    sepia_asm(&openImage, &asmImg);
    clock_t endASM = clock();
    double time_spent_asm = (double ) (endASM - beginASM) / CLOCKS_PER_SEC;
    printf("asm execution %f", time_spent_asm);


    char *outputFileName = argv[2];
    FILE *out = 0;
    enum write_status writeBMPStatus = writeBMP(outputFileName);

    if (writeBMPStatus == WRITE_OK) {
        out = fopen(outputFileName, "wb");
    } else {
        fclose(out);
        exit(1);
    }
    enum write_status writeImageStatus = toBMP(out, &cSepiaImage);
    if (writeImageStatus != WRITE_OK) {
        free(cSepiaImage.data);
        fprintf(stderr, "Error: can not write image");
        exit(1);

    }

    fclose(out);

    FILE *outASM = 0;
    char* outputASMFileName = "asm_out.bmp";
     writeBMPStatus = writeBMP(outputASMFileName);

    if (writeBMPStatus == WRITE_OK) {
        outASM = fopen(outputASMFileName, "wb");
    } else {
        fclose(outASM);
        exit(1);
    }
    writeImageStatus = toBMP(outASM, &asmImg);
    if (writeImageStatus != WRITE_OK) {
        free(asmImg.data);
        fprintf(stderr, "Error: can not write image");
        exit(1);

    }

    free(cSepiaImage.data);
    return 0;
}
