#include "../include/image.h"
#include <stdlib.h>
#include <stdio.h>
struct image makeSepia(struct image imageToSepia) {
    struct image tempImage;
    const size_t WIDTH = imageToSepia.width;
    const size_t HEIGHT = imageToSepia.height;
    tempImage = createImage(WIDTH, HEIGHT);

    for (size_t i = 0; i < HEIGHT; i++) {
        for (size_t j = 0; j < WIDTH; j++) {


            uint8_t r = imageToSepia.data[(WIDTH - j - 1) * HEIGHT + i].red;
            uint8_t g = imageToSepia.data[(WIDTH - j - 1) * HEIGHT + i].green;
            uint8_t b = imageToSepia.data[(WIDTH - j - 1) * HEIGHT + i].blue;
            uint8_t newR ;
            int rValue = 0.393 * r + 0.769 * g + 0.189 * b;
            if ( rValue>255){
                newR = 255;
            } else{
                newR = (uint8_t)rValue;
            }
            uint8_t newG ;
            int gValue = 0.349 * r + 0.686 * g + 0.168 * b;
            if ( gValue>255){
                newG = 255;
            } else{
                newG = (uint8_t)gValue;
            }
            uint8_t newB ;
            int bValue = 0.272 * r + 0.534 * g + 0.131 * b;
            if ( bValue>255){
                newB = 255;
            } else{
                newB = (uint8_t)bValue;
            }



            if (newR==0){
                printf("%d %d %d\n",r,g,b);
            }
            struct pixel newPixel = {.red = newR, .blue =newB, .green = newG};
            tempImage.data[(WIDTH - j - 1) * HEIGHT + i] = newPixel;
        }
    }

    return tempImage;
}