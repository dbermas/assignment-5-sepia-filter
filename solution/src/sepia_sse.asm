
section .data
;the coefficients for the components
;xmm3 = b1c11|b1c21|b1c31|b2c11 => xmm3 = b2c21|b2c31|b3c11|b3c21
;xmm3 = b3c31|b4c11|b4c21|b4c31
;c11 c21 c31 c11
b1: dd 0.131, 0.168, 0.189, 0.131
g1: dd 0.534, 0.686, 0.769, 0.534
r1: dd 0.272, 0.349, 0.393, 0.272
;c21 c31 c11 c21
b2: dd 0.168, 0.189, 0.131, 0.168
g2: dd 0.686, 0.769, 0.534, 0.686
r2: dd 0.349, 0.393, 0.272, 0.349
;c31 c11 c21 c31
b3: dd 0.189, 0.131, 0.168, 0.189
g3: dd 0.769, 0.534, 0.686, 0.769
r3: dd 0.393, 0.272, 0.349, 0.393

max_values_vec: dd 255, 255, 255, 255

section .text

global sepia_asm

sepia_asm:
    push r12
    push r13
    mov r12, [rdi + 16]
    mov r13, [rsi + 16]
    movups xmm6, [max_values_vec];for compare

    mov r8, [rdi] ;height
    mov r9, [rdi + 8] ; width
    imul r8, r9
    sub r8, 4
    push r8
    push 0
    .loop:
        mov rdi, [rsp]
        cmp rdi, [rsp + 8]
        ja .end

        imul rdi, 3
        mov rsi, rdi
        add rsi, r12
        add rdi, r13

         mov al, [rsi + 3]
        pinsrb xmm0, al, 3
        mov al, [rsi]
        pinsrb xmm0, al, 0
        pinsrb xmm0, al, 1
        pinsrb xmm0, al, 2

        pmovzxbd xmm0, xmm0
        cvtdq2ps xmm0, xmm0


        mov al, [rsi + 4]
        pinsrb xmm1, al, 3
        mov al, [rsi + 1]
        pinsrb xmm1, al, 0
        pinsrb xmm1, al, 1
        pinsrb xmm1, al, 2

        pmovzxbd xmm1, xmm1
        cvtdq2ps xmm1, xmm1


        mov al, [rsi + 5]
        pinsrb xmm2, al, 3
        mov al, [rsi + 2]
        pinsrb xmm2, al, 0
        pinsrb xmm2, al, 1
        pinsrb xmm2, al, 2

        pmovzxbd xmm2, xmm2
        cvtdq2ps xmm2, xmm2



        movdqu xmm3, [b1]
        movdqu xmm4, [g1]
        movdqu xmm5, [r1]

        mulps xmm0, xmm3
        mulps xmm1, xmm4
        mulps xmm2, xmm5

        addps xmm0, xmm1
        addps xmm0, xmm2

        cvtps2dq xmm0, xmm0
        pminud xmm0, xmm6

        pextrb [rdi], xmm0, 0
        pextrb [rdi + 1], xmm0, 4
        pextrb [rdi + 2], xmm0, 8
        pextrb [rdi + 3], xmm0, 12


        mov al, [rsi + 3]
        pinsrb xmm0, al, 1
        pinsrb xmm0, al, 0
        mov al, [rsi + 6]
        pinsrb xmm0, al, 3
        pinsrb xmm0, al, 2

        pmovzxbd xmm0, xmm0
        cvtdq2ps xmm0, xmm0

        mov al, [rsi + 4]
        pinsrb xmm1, al, 1
        pinsrb xmm1, al, 0
        mov al, [rsi + 7]
        pinsrb xmm1, al, 3
        pinsrb xmm1, al, 2

        pmovzxbd xmm1, xmm1
        cvtdq2ps xmm1, xmm1

        mov al, [rsi + 5]
        pinsrb xmm2, al, 1
        pinsrb xmm2, al, 0
        mov al, [rsi + 8]
        pinsrb xmm2, al, 3
        pinsrb xmm2, al, 2

        pmovzxbd xmm2, xmm2
        cvtdq2ps xmm2, xmm2


        movdqu xmm3, [b2]
        movdqu xmm4, [g2]
        movdqu xmm5, [r2]

        mulps xmm0, xmm3
        mulps xmm1, xmm4
        mulps xmm2, xmm5

        addps xmm0, xmm1
        addps xmm0, xmm2

        cvtps2dq xmm0, xmm0
        pminud xmm0, xmm6

        pextrb [rdi + 4], xmm0, 0
        pextrb [rdi + 5], xmm0, 4
        pextrb [rdi + 6], xmm0, 8
        pextrb [rdi + 7], xmm0, 12


        mov al, [rsi + 6]
        pinsrb xmm0, al, 0
        mov al, [rsi + 9]
        pinsrb xmm0, al, 3
        pinsrb xmm0, al, 1
        pinsrb xmm0, al, 2

        pmovzxbd xmm0, xmm0
        cvtdq2ps xmm0, xmm0

        mov al, [rsi + 7]
        pinsrb xmm1, al, 0
        mov al, [rsi + 10]
        pinsrb xmm1, al, 3
        pinsrb xmm1, al, 1
        pinsrb xmm1, al, 2

        pmovzxbd xmm1, xmm1
        cvtdq2ps xmm1, xmm1

        mov al, [rsi + 8]
        pinsrb xmm2, al, 0
        mov al, [rsi + 11]
        pinsrb xmm2, al, 3
        pinsrb xmm2, al, 1
        pinsrb xmm2, al, 2

        pmovzxbd xmm2, xmm2
        cvtdq2ps xmm2, xmm2

        movdqu xmm3, [b3]
        movdqu xmm4, [g3]
        movdqu xmm5, [r3]

        mulps xmm0, xmm3
        mulps xmm1, xmm4
        mulps xmm2, xmm5

        addps xmm0, xmm1
        addps xmm0, xmm2

        cvtps2dq xmm0, xmm0
        pminud xmm0, xmm6

        pextrb [rdi + 8], xmm0, 0
        pextrb [rdi + 9], xmm0, 4
        pextrb [rdi + 10], xmm0, 8
        pextrb [rdi + 11], xmm0, 12

        mov rdi, [rsp]
        add rdi, 4
        mov [rsp], rdi
        jmp .loop
    .end:
        add rsp, 16
        pop r13
        pop r12
        ret