#include "../include/bmp.h"
#include "../include/bmpHeader.h"
#include "../include/serializer.h"
#include <stdio.h>


#define BYTES_PER_PIXEL 3

#ifndef SERIALIZER_C
#define SERIALIZER_C

enum write_status toBMP(FILE *out, struct image const *img) {
    const uint8_t padding = countPadding(img->width * sizeof(struct pixel));
    struct bmpHeader header = fillHeader(img);
    fwrite(&header, sizeof(struct bmpHeader), 1, out);

    if (ferror(out)) {
        return WRITE_ERROR;
    }
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(&img->data[i * img->width], BYTES_PER_PIXEL, img->width, out) != img->width)
            return WRITE_ERROR;
        for (uint8_t j = 0; j < padding; j++) {
            if (fputc(0, out) == EOF) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

#endif
